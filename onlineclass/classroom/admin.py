from django.contrib import admin
from .models import Classroom, User
from django.contrib.auth.admin import UserAdmin


# Register your models here.

admin.site.register(Classroom)
admin.site.register(User)