from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
# Create your models here.
CLASS_CHOICES = ( 
    ("V", "V"), 
    ("VI", "VI"), 
    ("VII", "VII"), 
    ("VIII", "VIII"), 
    ("IX", "IX"), 
    ("X", "X"), 
    ("XI", "XI"), 
    ("X", "X"), 
)
class User(AbstractUser):

    is_student = models.BooleanField(default=False)
    is_teacher = models.BooleanField(default=False)


class Classroom(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    room_name = models.CharField(max_length=20)
    subject = models.CharField(max_length=25)
    standard = models.CharField(max_length = 4, 
        choices = CLASS_CHOICES, 
        default = 'V'
        )
    description = models.TextField()
     
    class Meta:
        verbose_name = 'Classroom'
        verbose_name_plural = 'Classrooms'

    def __str__(self):
         return self.room_name

