from django.urls import path
from . import views

app_name = 'cr'

urlpatterns = [
    path('', views.home, name='home'),
    path('classroom/<int:id>', views.class_details, name = 'class_details'),
    path('classroom/<int:id>/register', views.register, name = 'register'),
    path('classroom/<int:id>/register_cancel', views.register_cancel, name = 'register_cancel'),
]
