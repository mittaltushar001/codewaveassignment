# Description
The features included in the project are:
Teacher can create classrooms.
Students can view the classroom and register. Students will receive an email after successful sign up and also after registering to a course. Register option is only available if the student is logged in. Students can see registered classes. Students can also cancel a registration.
# How to run
Create virtual environment: python -m venv {environment name}.
After cloning run the command: pip install -r requirements.txt.
 
Run: 
1. python manage.py makemigrations
2. python manage.py migrate
Create superuser:
3. python manage.py createsuperuser
4. python manage.py runserver

To add teacher:
Go to django admin panel and go to user table. Fill in the required fields,Set 'is_teacher' to true.
To add classroom:
Go to Classroom table in the django admin panel.

# Directory Structure
    .
    ├── classroom
    │   ├── admin.py
    │   ├── apps.py
    │   ├── __init__.py
    │   ├── models.py
    │   ├── templates
    │   │   ├── class_details.html
    │   │   └── home.html
    │   ├── tests.py
    │   ├── urls.py
    │   └── views.py
    ├── manage.py
    ├── onlineclass
    │   ├── asgi.py
    │   ├── __init__.py
    │   ├── settings.py
    │   ├── urls.py
    │   └── wsgi.py
    ├── README.md
    ├── requirements.txt
    ├── student
    │   ├── admin.py
    │   ├── apps.py
    │   ├── forms.py
    │   ├── __init__.py
    │   ├── models.py
    │   ├── templates
    │   │   ├── login.html
    │   │   ├── my_classes.html
    │   │   └── signup.html
    │   ├── tests.py
    │   ├── urls.py
    │   └── views.py
    └── templates
        └── base.html
